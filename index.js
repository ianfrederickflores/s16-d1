/*While Loop
	-takes in an expression/condition, if the condition evaluates to true the statements inside the code block will be executed.
	Syntax:
	while (expression/condition){
		statement/s;
	}
*/
let count = 5;
while (count >= 0){
	console.log("While: " + count);
	count--;
}

let numA = 0;
while (numA < 10){
	console.log("While " + numA)
	numA++
}

/* Do While Loop
	-a do-while loop works alot like the while loop. But unlike the while loop, do-while loops guarantee that the code will be executed at least once.
	Syntax:
		do{
			statement/s;
		} while(expression/condition)		 
*/

let number = Number(prompt("Give me a number"));
do{
	console.log("Do While " + number);
	number += 1;
} while (number < 10)

/*For Loop
	-is more flexible than while and do-while loops. It consist of three parts:
	1. initialization - a value that will track the progression of the loop.
	2. expression/condition - determines whether the loop will run one more time.
	3. finalExpression - indicates how to advance the loop.
	
	Syntax:
	for(initialization; expression/condition; finalExpression){
		statement/s
	}
*/


for (let count3 = 0; count3 <= 3; count3++){
	console.log(count3)
}

console.log("Another example: ");
let myString = "alex";
// .length - aproperty that returns the total number of characters in a string
console.log(myString.length);

console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);

console.log("Word Loop")
for(let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}

console.log("Vowel Count Example")
let myName = "AlEx";
for(let i = 0; i < myName.length; i++){
	console.log(myName[i].toLowerCase());
	if(
		myName[i].toLowerCase == "a" ||
		myName[i].toLowerCase == "e" ||
		myName[i].toLowerCase == "i" ||
		myName[i].toLowerCase == "o" ||
		myName[i].toLowerCase == "u" 
	){
		console.log(3);
	}
	
	else{
		console.log(myName[i])
	}
}

/* Continue and Break Statements
	continue - a statement that allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block.
	break - a statement that is used to terminate the current loop once a match has been found
*/

for(let count4 = 0; count4 <= 20; count4++){
	if(count4 % 2 === 0){
		continue;
	}

	console.log("Continue and Break: " + count4);
	if(count4 > 10){
		break;
	}
}

console.log("Iterating the length of the String")

let name = "alexandro"

for(let i = 0; i < name.length; i++){
	console.log(name[i]);

	if(name[i].toLowerCase() === "a"){
		console.log("Continue to next iteration");
		continue
	}

	if(name[i] === "o"){
		break;
	}
}
